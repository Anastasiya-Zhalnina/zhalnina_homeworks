import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int min = a % 10;
        while (a != -1) {
            if (a == 0) {
                min = 0;
            } else {
                while (a != 0) {
                    int mod = a % 10;
                    if (mod < min) {
                        min = mod;
                    }
                    a = a / 10;
                }
            }
            a = scanner.nextInt();
        }
        System.out.println(min);
    }
}
