import java.time.LocalDateTime;

public class Logger {

    private static final Logger instance;
    private LocalDateTime loggerCreationTime;

    static {
        instance = new Logger();
    }

    private Logger() {
        loggerCreationTime = LocalDateTime.now();
    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println("Logger [" + loggerCreationTime + "] текущее сообщение - " + message);
    }
}
