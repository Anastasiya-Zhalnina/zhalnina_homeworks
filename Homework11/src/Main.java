public class Main {
    private static final int MAX_COUNT_OPERATIONS = 5;

    public static void main(String[] args) {
        Logger loggerFirst = Logger.getInstance();
        loggerFirst.log("Запуск...");
        for (int i = 1; i <= MAX_COUNT_OPERATIONS; i++) {
            loggerFirst.log("Этап №" + i);
        }

        Logger loggerSecond = Logger.getInstance();
        loggerSecond.log("Видим, что Logger был создан в одном экземпляре");

        Logger loggerThird = Logger.getInstance();
        loggerThird.log("Завершение работы");
    }
}
