package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    class ForGcd {
        @ParameterizedTest(name = "gcd = {2} on number a = {0} and b = {1}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "64, 48, 16", "0, 35, 35", "10, 0, 10"})
        public void return_correct_result(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on a = {0} and b = {1}")
        @CsvSource(value = {"-10, -20", "-10, 1", "2, -5"})
        public void bad_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}