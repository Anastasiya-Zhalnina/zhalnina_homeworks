### Задача
```
Установить JDK

1) Выполняете в консоли:

javac -version
java -version

2) Копируете вывод из консоли и вставляете в ответ в качестве ДЗ.
```
### Решение
```
Microsoft Windows [Version 10.0.19043.1288]
(c) Корпорация Майкрософт (Microsoft Corporation). Все права защищены.

C:\Users\79140>java -version
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)

C:\Users\79140>javac -version
javac 1.8.0_201

C:\Users\79140>
```