### Дополнительное задание, не на проверку - написать предыдущие программы на Java.
### Задача №1
```
Пусть есть последовательность чисел a0, a1, a2, ..., aN
N-> infinity, aN -> -1

Вывести сумму всех чисел последовательности.
```
### Решение на языке Java №1
```
import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int sum = 0;
        while (a != -1) {
            sum += a;
            a = scanner.nextInt();
        }
        System.out.println(sum);
    }
}
```
### Задача №2
```
Пусть есть последовательность чисел a0, a1, a2, ..., aN
N-> infinity, aN -> -1

Посчитать количество всех четных чисел и количество нечетных чисел.
```
### Решение на языке Java №2
```
import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int eventCount = 0;
        int oddsCount = 0;
        while (a != -1) {
            if (a % 2 == 0) {
                eventCount++;
            } else {
                oddsCount++;
            }
            a = scanner.nextInt();
        }
        System.out.println("Number of even numbers  = " + eventCount);
        System.out.println("Number of odd numbers = " + oddsCount);
    }
}
```
### Задача №3
```
Пусть есть последовательность чисел a0, a1, a2, ..., aN
N-> infinity, aN -> -1

Найти минимум среди всех чисел последовательности.
```
### Решение на языке Java №3
```
import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int min = a;
        while (a != -1) {
            if (min > a) {
                min = a;
            }
            a = scanner.nextInt();
        }
        System.out.println("min = " + min);
    }
}
```
### Задача №4
```
Пусть есть последовательность чисел a0, a1, a2, ..., aN
N-> infinity, aN -> -1

Вывести минимальную сумму цифр для четных чисел (всче числа положительные), числа в диапазоне от 0 до 9999.
```
### Решение на языке Java №4
```
import java.util.Scanner;

public class Program4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int digitsSum = 0;
        int minDigitsSum = 37;
        while (a != -1) {
            digitsSum = 0;
            if (a % 2 == 0) {
                while (a != 0) {
                    int lastDigit = a % 10;
                    digitsSum += lastDigit;
                    a = a / 10;
                }
                if (digitsSum < minDigitsSum) {
                    minDigitsSum = digitsSum;
                }
            }
            a = scanner.nextInt();
        }
        System.out.println(minDigitsSum);
    }
}
```
### Задача№5

```
Пусть есть последовательность чисел 
a0, a1, a2, ..., aN
N -> infinity, aN -> -1

Вывести количество локальных минимумов:
ai - локальный минимум, если ai-1 > ai < ai+1
```
### Решение на языке Java №5
```
import java.util.Scanner;

public class Program5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        int a = scanner.nextInt();
        int previous = a;
        int next;
        while (a != -1) {
            next = scanner.nextInt();
            if (a < previous && a < next) {
                count++;
            }
            previous = a;
            a = next;
        }
        System.out.println(count);
    }
}
```