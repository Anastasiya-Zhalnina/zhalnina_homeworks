create table Product
(
    id          serial primary key,
    description varchar(300),
    price       double precision,
    count       integer
);

insert into Product(description, price, count)
values ('Браслет  из сердолика', 1500, 5);
insert into Product(description, price, count)
values ('Браслет из натурального жемчуга', 1700, 3);
insert into Product(description, price, count)
values ('Серьги из коралла', 1000, 10);
insert into Product(description, price, count)
values ('Браслет из качественной фурнитуры', 950, 8);
insert into Product(description, price, count)
values ('Браслет из качественной фурнитуры', 1000, 5);
insert into Product(description, price, count)
values ('Браслет из качественной фурнитуры', 1200, 3);
insert into Product(description, price, count)
values ('Серьги из черного агата и перламутра', 800, 2);
insert into Product(description, price, count)
values ('Браслет из черного агата', 1500, 3);
insert into Product(description, price, count)
values ('Серьги-гвоздики из коралла', 1200, 4);
insert into Product(description, price, count)
values ('Серьги из черного агата с использованием родированной фурнитуры', 1100, 2);
insert into Product(description, price, count)
values ('Серьги из агата, коралл, шпинель, фурнитуры класса люкс', 1300, 8);
insert into Product(description, price, count)
values ('Серьги-гвоздики из тонированного агата класса люкс', 1500, 2);
insert into Product(description, price, count)
values ('Серьги с карамельным сердоликом и родированной фурнитуры', 1200, 5);
insert into Product(description, price, count)
values ('Браслет из дымчатого кварца-раухтапаза и фурнитуры класса люкс', 2500, 3);
insert into Product(description, price, count)
values ('Браслет из сердолика и фурнитуры класса люкс', 2500, 1);

create table Customer
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20)
);

insert into Customer(first_name, last_name)
values ('Ксения', 'Сафонова');
insert into Customer(first_name, last_name)
values ('Софья', 'Крюкова');
insert into Customer(first_name, last_name)
values ('Екатерина', 'Исакова');
insert into Customer(first_name, last_name)
values ('Василиса', 'Зверева');
insert into Customer(first_name, last_name)
values ('Ксения', 'Петрова');


create table Orders
(
    product_id  integer,
    customer_id integer,
    date        timestamp,
    count       integer,
    foreign key (product_id) references Product (id),
    foreign key (customer_id) references Customer (id)
);

insert into Orders(product_id, customer_id, date, count)
values (1, 5, '2020-06-24 06:32:50', 2);
insert into Orders(product_id, customer_id, date, count)
values (6, 4, '2021-01-06 13:03:36', 1);
insert into Orders(product_id, customer_id, date, count)
values (3, 3, '2021-03-17 20:26:52', 2);
insert into Orders(product_id, customer_id, date, count)
values (3, 2, '2021-07-24 23:50:22', 1);
insert into Orders(product_id, customer_id, date, count)
values (15, 1, '2021-11-26 22:44:20', 1);
insert into Orders(product_id, customer_id, date, count)
values (9, 1, '2021-11-23 22:44:20', 1);

-- Список товаров, которые были заказаны
select p.description
from Product as p
         inner join Orders as o on p.id = o.product_id;

-- ФИ заказчика и товар который он приобрел
select c.last_name, c.first_name, d.description
from Customer as c
         left join (select p.description, o.customer_id
                    from Product as p
                             inner join Orders as o on p.id = o.product_id) as d on c.id = d.customer_id;

-- Описание товара и в каком количестве он был заказан
select p.description, c.count
from Product as p
         inner join (select product_id, sum(count) as count from Orders group by product_id) as c
                    on p.id = c.product_id;

-- Вывести продукты которые были заказаны в определенном количестве
select *
from Product as p
         left join (select product_id, sum(count) as count from Orders group by product_id) as o on p.id = o.product_id
where o.count = 1;