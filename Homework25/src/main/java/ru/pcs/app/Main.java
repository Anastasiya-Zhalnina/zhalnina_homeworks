package ru.pcs.app;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.pcs.repository.ProductsRepository;
import ru.pcs.repository.ProductsRepositoryJdbcTemplateImpl;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource(
                "jdbc:postgresql://localhost:5432/shop",
                "postgres",
                "e47Y6xLbdA");
        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findAllByPrice(2500));
        System.out.println(productsRepository.findAllByOrdersCount(3));
    }
}
