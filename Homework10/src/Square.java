public class Square extends Rectangle implements RelocatableFigure {

    public Square(double x, double y, double a) {
        super(x, y, a, a);
    }

    @Override
    public void movingFigure(double kX, double kY) {
        System.out.print("Начальные координаты квадрата: (" + this.x + "; " + this.y + ").");
        this.x = kX;
        this.y = kY;
        System.out.println(" Квадрат был перемещен в заданную точку. Новые координаты квадрата: (" + this.x + "; " + this.y + ").");
    }
}
