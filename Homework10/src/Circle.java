public class Circle extends Ellipse implements RelocatableFigure {

    public Circle(double x, double y, double a) {
        super(x, y, a, a);
    }

    @Override
    public void movingFigure(double kX, double kY) {
        System.out.print("Начальные координаты круга: (" + this.x + "; " + this.y + ").");
        this.x = kX;
        this.y = kY;
        System.out.println(" Круг был перемещен в заданную точку. Новые координаты круга: (" + this.x + "; " + this.y + ").");
    }
}
