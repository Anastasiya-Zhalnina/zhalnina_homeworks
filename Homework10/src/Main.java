public class Main {
    public static void main(String[] args) {
        RelocatableFigure[] relocatableFigure = new RelocatableFigure[2];
        Square square = new Square(2, 2, 4);
        Circle circle = new Circle(3, 4, 6);

        relocatableFigure[0] = square;
        relocatableFigure[1] = circle;

        for (int i = 0; i < relocatableFigure.length; i++) {
            relocatableFigure[i].movingFigure(3, 6);
        }
        System.out.println("************************************************************************************************************************");

        for (int i = 0; i < relocatableFigure.length; i++) {
            relocatableFigure[i].movingFigure(5, 5);
        }
    }
}
