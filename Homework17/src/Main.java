import java.util.*;

public class Main {
    public static void main(String[] args) {
        String s = "hello java hello university java hello friend learning java friend hello goodbye";
        Map<String, Integer> map = new HashMap<>();
        String[] words = s.split(" ");
        Integer value = 0;
        for (String key : words) {
            map.put(key, value);
        }
        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            String key = pair.getKey();
            value = pair.getValue();
            for (String element : words) {
                if (key.equals(element)) {
                    value++;
                }
            }
            System.out.println(key + " - " + value);
        }
    }
}
