import java.util.*;

public class MainForEach {
    public static void main(String[] args) {
        String s = "hello java hello university java hello friend cat learning java friend hello goodbye";
        Map<String, Integer> map = new HashMap<>();
        String[] words = s.split(" ");
        Integer count = 0;
        for (String key : words) {
            map.put(key, count);
        }

        map.forEach((key, value) -> {
            for (String element : words) {
                if (key.equals(element)) {
                    value++;
                }
            }
            System.out.println(key + " - " + value);
        });
    }
}
