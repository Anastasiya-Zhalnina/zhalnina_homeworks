import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println("Начальный массив: " + Arrays.toString(array));
        System.out.println("Заданное число находится под индексом: " + getIndexOfNumber(array, 18));
        moveToTheLeft(array);
        System.out.println("Массив после сдвига значимых элементов влево: " + Arrays.toString(array));
    }

    public static int getIndexOfNumber(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static void moveToTheLeft(int[] array) {
        int[] arrayCopy = new int[array.length];
        int i = 0;
        for (int j = 0; j < array.length; j++) {
            if (array[j] != 0) {
                arrayCopy[i] = array[j];
                i++;
            }
        }

        for (int k = 0; k < array.length; k++) {
            array[k] = arrayCopy[k];
        }
    }
}
