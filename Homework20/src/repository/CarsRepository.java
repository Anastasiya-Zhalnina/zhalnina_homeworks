package repository;

import model.Car;

import java.util.List;

public interface CarsRepository {
    /**
     * Поиск всех автомобилей
     *
     * @return список всех автомобилей
     */
    List<Car> findAllCars();

    /**
     * Поиск номеров автомобилей по цвету или пробегу
     *
     * @param color   - цвет автомобиля
     * @param mileage - пробег автомобиля
     * @return список номеров найденных автомобилей
     */
    List<String> findCarNumbersByColorOrMileage(String color, Long mileage);

    /**
     * Количество уникальных автомобилей в заданном ценовом диапазоне
     *
     * @param costStart - нижняя граница цены
     * @param costEnd   - верхняя граница цены
     * @return количество уникальных автомобилей в заданном диапазоне
     */
    Long countOfUniqueCarsByCost(Long costStart, Long costEnd);

    /**
     * Поиск цвета автомобиля с минимальной стоимостью
     *
     * @return цвет автомобиля
     */
    String findCarColorWithMinCoast();

    /**
     * Средняя стоимость заданной модели автомобиля
     *
     * @param model - модель автомобиля
     * @return средняя стоимость заданной модели автомобиля
     */
    Double AverageCostOfCarModel(String model);
}
