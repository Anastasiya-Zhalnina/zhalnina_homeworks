package repository;

import model.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryImpl implements CarsRepository {

    private String fileName;

    private static final Function<String, Car> carMapper = line -> {
        String[] parseLine = line.split("\\|");
        return new Car(parseLine[0], parseLine[1], parseLine[2], Long.parseLong(parseLine[3]), Long.parseLong(parseLine[4]));
    };

    public CarsRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAllCars() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(carMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<String> findCarNumbersByColorOrMileage(String color, Long mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage))
                    .map(Car::getNumber)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Long countOfUniqueCarsByCost(Long costStart, Long costEnd) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getCost() >= costStart && car.getCost() <= costEnd)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String findCarColorWithMinCoast() {
        String color = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            Optional<Car> car = reader
                    .lines()
                    .map(carMapper)
                    .min(Comparator.comparingLong(Car::getCost));
            if (car.isPresent()) {
                color = car.get().getColor();
            }
            return color;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Double AverageCostOfCarModel(String model) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Long> costs = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getModel().equals(model))
                    .map(Car::getCost)
                    .collect(Collectors.toList());
            OptionalDouble average = costs.stream().mapToDouble(cost -> cost).average();
            if (average.isPresent()) {
                return average.getAsDouble();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
