package app;

import repository.CarsRepository;
import repository.CarsRepositoryImpl;

public class Main {

    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryImpl("cars_db.txt");

        System.out.println("                  СИСОК ВСЕХ АВТОМОБИЛЕЙ");
        System.out.printf("%17s|%22s|%6s|%7s|%10s|\n", "НОМЕР АВТОМОБИЛЯ", "МОДЕЛЬ", "ЦВЕТ", "ПРОБЕГ", "СТОИМОСТЬ");
        carsRepository.findAllCars().forEach(System.out::println);
        System.out.println();

        System.out.print("НОМЕРА ВСЕХ АВТОМОБИЛЕЙ ИМЕЮЩИХ ЗАДАННЫЙ ЦВЕТ ИЛИ ПРОБЕГ: ");
        carsRepository.findCarNumbersByColorOrMileage("Black", 0L).forEach(number -> System.out.print(number + " "));
        System.out.println();

        System.out.println("КОЛИЧЕСТВО УНИКАЛЬНЫХ МОДЕЛЕЙ В ЗАДАННОМ ДИАПОЗОНЕ РАВНО: " + carsRepository.countOfUniqueCarsByCost(700_000L, 800_000L) + " ШТ.");

        System.out.println("ЦВЕТ АВТОМОБИЛЯ С МИНИМАЛЬНОЙ СТОИМОСТЬЮ: " + carsRepository.findCarColorWithMinCoast());

        System.out.printf("СРЕДНЯЯ СТОИМОСТЬ ЗАДАННОЙ МОДЕЛИ: %.2f\n", carsRepository.AverageCostOfCarModel("Toyota Camry"));
    }
}
