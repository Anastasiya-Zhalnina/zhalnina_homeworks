public class Main {

    public static void main(String[] args) {
        Figure[] figures = new Figure[5];
        Figure figure = new Figure(5,3);
        Ellipse ellipse = new Ellipse(5, 6, 5, 6);
        Circle circle = new Circle(8, 6, 8);
        Rectangle rectangle = new Rectangle(10, 20, 10, 20);
        Square square = new Square(9, 6, 9);

        figures[0] = figure;
        figures[1] = ellipse;
        figures[2] = circle;
        figures[3] = rectangle;
        figures[4] = square;

        for (int i = 0; i < figures.length; i++) {
            System.out.printf("Периметр заданной фигуры равен: %4.2f \n", figures[i].getPerimeter());
        }
    }
}
