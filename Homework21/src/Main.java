import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int array[];
    public static int sums[];
    public static int sum;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество чисел для суммирования: ");
        int numbersCount = scanner.nextInt();

        System.out.print("Введите количество потоков для выполнения суммирования чисел: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }


        int realSum = 0;
        for (int number : array) {
            realSum += number;
        }
        System.out.println("Сумма элементов вычисленная не в потоках: " + realSum);
        System.out.println("=====================================================");

        int from = 0;
        int to = numbersCount / threadsCount + numbersCount % threadsCount;
        for (int i = 0; i < threadsCount; i++) {
            Thread thread = new SumThread(from, to);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
            System.out.println("Сумма элементов в потоке " + thread.getName() + " равна " + sums[sum]);
            sum++;
            from = to;
            to = to + numbersCount / threadsCount;
        }


        int byThreadSum = 0;
        for (int sum : sums) {
            byThreadSum += sum;
        }
        System.out.println("=====================================================");
        System.out.println("Сумма элементов вычисленная в потоках: " + byThreadSum);
    }
}
