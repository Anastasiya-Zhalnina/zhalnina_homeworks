public class SumThread extends Thread {
    private int from;
    private int to;

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int i = from; i < to; i++) {
            sum += Main.array[i];
        }
        Main.sums[Main.sum] = sum;
    }
}
