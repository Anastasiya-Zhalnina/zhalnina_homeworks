package app;

import model.User;
import repository.UsersRepository;
import repository.UsersRepositoryFileImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        System.out.println("              Список пользователей:");
        System.out.printf("%4s|%10s| %7s|%17s\n", "№", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersRepository.findAll()) {
            System.out.print(user);
        }
        System.out.println("**************************************************");

        System.out.println("   Пользователь, найденный по заданному id:");
        System.out.printf("%4s|%10s| %7s|%17s\n", "№", "Имя", "Возраст", "Работающий да/нет");
        System.out.print(usersRepository.findById(8));
        System.out.println("**************************************************");

        usersRepository.update(new User(1, "Сергей", 27, true));
        usersRepository.update(new User(3, "Иван", 35, false));
        System.out.println("      Обновленный список пользователей:");
        System.out.printf("%4s|%10s| %7s|%17s\n", "№", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersRepository.findAll()) {
            System.out.print(user);
        }
        System.out.println("**************************************************");

        List<User> usersFoundByAge = usersRepository.findByAge(39);
        System.out.println("Пользователи, соответствующие заданному возрасту:");
        System.out.printf("%4s|%10s| %7s|%17s\n", "№", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersFoundByAge) {
            System.out.print(user);
        }

        System.out.println("**************************************************");
        List<User> usersIsWorking = usersRepository.findByIsWorkerIsTrue();
        System.out.println("          Работающие пользователи:");
        System.out.printf("%4s|%10s| %7s|%17s\n", "№", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersIsWorking) {
            System.out.print(user);
        }
    }
}
