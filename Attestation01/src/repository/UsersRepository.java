package repository;

import model.User;

import java.util.List;

public interface UsersRepository {
    /**
     * Поиск всех пользователей в файле
     * @return список пользователей
     */
    List<User> findAll();

    /**
     * Сохранение нового пользователя в файл
     * @param user - новый пользователь
     */
    void save(User user);

    /**
     * Поиск пользователя по возрасту
     * @param age возраст пользователя
     * @return список пользователей, соответствующего возраста
     */
    List<User> findByAge(int age);

    /**
     * Поиск работающих пользователей
     * @return список работающих пользователей
     */
    List<User> findByIsWorkerIsTrue();

    /**
     * Поиск пользователя по заданному id
     * @param id - id пользователя
     * @return найденный пользователь, соответствующий заданному id
     */
    User findById(int id);

    /**
     * Обновление данных пользователя в файле
     * @param user - пользователь, данные которого необходимо обновить
     */
    void update(User user);
}
