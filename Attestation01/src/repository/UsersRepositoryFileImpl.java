package repository;

import model.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        BufferedReader bufferedReader;
        try (Reader reader = new FileReader(fileName)) {
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User user) {
        BufferedWriter bufferedWriter;
        try (Writer writer = new FileWriter(fileName, true)) {
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> foundUsers = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getAge() == age) {
                foundUsers.add(user);
            }
        }
        return foundUsers;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> foundUsers = new ArrayList<>();
        for (User user : findAll()) {
            if (user.isWorker()) {
                foundUsers.add(user);
            }
        }
        return foundUsers;
    }

    @Override
    public User findById(int id) {
        for (User user : findAll()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
        List<String> listUser = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                listUser.add(line + "\n");
            }
            listUser.removeIf(users -> users.equals("" + "\n"));
            String newUser = user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker() + "\n";
            for (String element : listUser) {
                String[] argumentsUser = element.split("\\|");
                if (Integer.parseInt(argumentsUser[0]) == user.getId()) {
                    listUser.set(listUser.indexOf(element), newUser);
                }
            }
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                for (String element : listUser) {
                    writer.write(element);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
