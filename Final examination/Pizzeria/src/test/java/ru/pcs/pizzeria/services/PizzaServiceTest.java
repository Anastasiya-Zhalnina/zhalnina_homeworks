package ru.pcs.pizzeria.services;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.pcs.pizzeria.forms.PizzaForm;
import ru.pcs.pizzeria.model.Pizza;
import ru.pcs.pizzeria.repository.PizzaRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * pizzeria
 * 11.12.2021
 *
 * @author Anastasiya Zhalnina
 */

@DisplayName(value = "Pizza Service is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@SpringBootTest
class PizzaServiceTest {
    @MockBean
    private PizzaRepository pizzaRepository;

    @Autowired
    private PizzaService pizzaService = new PizzaServiceImpl(pizzaRepository);
    private final List<Pizza> pizzas = new ArrayList<>();
    private Pizza pizzaFirst;
    private Pizza pizzaSecond;

    @BeforeEach
    public void init() {
        pizzaFirst = new Pizza();
        pizzaFirst.setName("Первая пицца");
        pizzaFirst.setIngredients("Ингредиенты первой пиццы");
        pizzaFirst.setPrice(500);
        pizzaFirst.setImage("Изображение первой пиццы");

        pizzaSecond = new Pizza();
        pizzaSecond.setId(2);
        pizzaSecond.setName("Первая пицца");
        pizzaSecond.setIngredients("Ингредиенты первой пиццы");
        pizzaSecond.setPrice(500);
        pizzaSecond.setImage("Изображение первой пиццы");

        pizzas.add(pizzaFirst);
        pizzas.add(pizzaSecond);
    }

    @Test
    public void the_pizza_list_must_be_returned() {
        Mockito.when(pizzaRepository.findAll()).thenReturn(pizzas);
        List<Pizza> foundPizzas = pizzaService.getAllPizzas();
        assertNotNull(foundPizzas);
        assertEquals(2, foundPizzas.size());
    }

    @Test
    public void returning_pizza_by_id() {
        Mockito.when(pizzaRepository.findById(2)).thenReturn(java.util.Optional.ofNullable(pizzaSecond));
        Pizza found = pizzaService.getPizza(2);
        assertNotNull(found);
        assertEquals(pizzaSecond.getName(), found.getName());
        assertEquals(pizzaSecond.getId(), found.getId());
    }

    @Test
    public void delete_pizza_by_id() {
        pizzaService.deletePizza(pizzaSecond.getId());
        Mockito.verify(pizzaRepository, Mockito.times(1))
                .deleteById(pizzaSecond.getId());
    }

    @Test
    public void update_pizza() {
        Mockito.when(pizzaRepository.save(pizzaFirst)).thenReturn(pizzaFirst);
        pizzaFirst.setName("Мексиканская");
        pizzaService.updatePizza(pizzaFirst);
        assertNotNull(pizzaFirst);
        assertEquals(pizzaFirst.getName(), "Мексиканская");
    }

    @Test
    public void save_pizza() {
        Mockito.when(pizzaRepository.save(pizzaFirst)).thenReturn(pizzaFirst);
        PizzaForm pizzaForm = new PizzaForm();
        pizzaForm.setName("Первая пицца");
        pizzaForm.setIngredients("Ингредиенты первой пиццы");
        pizzaForm.setPrice(500);
        pizzaForm.setImage("Изображение первой пиццы");
        pizzaService.addPizza(pizzaForm);
        assertNotNull(pizzaFirst);
        assertEquals(pizzaForm.getName(), pizzaFirst.getName());
        assertEquals(pizzaForm.getIngredients(), pizzaFirst.getIngredients());
        assertEquals(pizzaForm.getPrice(), pizzaFirst.getPrice());
        assertEquals(pizzaForm.getImage(), pizzaFirst.getImage());
    }
}