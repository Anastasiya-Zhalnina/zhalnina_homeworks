package ru.pcs.pizzeria.services;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.pcs.pizzeria.model.Basket;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.model.Pizza;
import ru.pcs.pizzeria.repository.BasketRepository;
import ru.pcs.pizzeria.repository.CustomerRepository;
import ru.pcs.pizzeria.repository.PizzaRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * pizzeria
 * 12.12.2021
 *
 * @author Anastasiya Zhalnina
 */
@DisplayName(value = "Basket Service is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@SpringBootTest
class BasketServiceTest {
    @MockBean
    private BasketRepository basketRepository;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private PizzaRepository pizzaRepository;

    @Autowired
    private BasketService basketService = new BasketServiceImpl(basketRepository, customerRepository, pizzaRepository);
    private final List<Basket> baskets = new ArrayList<>();
    private Basket basketFirst;
    private Basket basketSecond;
    private Customer customerFirst;
    private Customer customerSecond;
    private Pizza pizzaSecond;

    @BeforeEach
    public void init() {
        basketFirst = new Basket();
        basketFirst.setId(1);
        customerFirst = new Customer();
        customerFirst.setId(1);
        basketFirst.setCustomer(customerFirst);
        Pizza pizzaFirst = new Pizza();
        pizzaFirst.setId(1);
        basketFirst.setPizza(pizzaFirst);

        basketSecond = new Basket();
        basketSecond.setId(2);
        customerSecond = new Customer();
        customerSecond.setId(2);
        basketFirst.setCustomer(customerSecond);
        pizzaSecond = new Pizza();
        pizzaSecond.setId(2);
        basketFirst.setPizza(pizzaSecond);

        baskets.add(basketFirst);
    }

    @Test
    public void save_basket() {
        Mockito.when(basketRepository.save(basketSecond)).thenReturn(basketSecond);
        Basket basket = new Basket();
        basket.setId(2);
        basket.setCustomer(customerSecond);
        basket.setPizza(pizzaSecond);
        basketService.addPizzaToBasket(customerSecond.getId(), pizzaSecond.getId());
        assertNotNull(basket);
        assertEquals(basket.getCustomer(), customerSecond);
        assertEquals(basket.getPizza(), pizzaSecond);
    }

    @Test
    public void delete_basket_by_id() {
        basketService.deleteBasket(basketFirst.getId());
        Mockito.verify(basketRepository, Mockito.times(1))
                .deleteById(basketFirst.getId());
    }

    @Test
    public void delete_basket_by_customer_Id() {
        basketService.deleteBasketsByCustomerId(2);
        Mockito.verify(basketRepository, Mockito.times(1))
                .deleteBasketsByCustomer_Id(customerSecond.getId());
    }

    @Test
    public void returning_basket_by_customer_id() {
        Mockito.when(basketRepository.findAllByCustomer_Id(1)).thenReturn(baskets);
        List<Basket> found = basketService.getBasketByCustomer(customerFirst.getId());
        assertNotNull(found);
        assertEquals(1, found.size());
    }

    @Test
    public void the_basket_list_must_be_returned() {
        Mockito.when(basketRepository.findAll()).thenReturn(baskets);
        List<Basket> foundPizzas = basketService.getAllBaskets();
        assertNotNull(foundPizzas);
        assertEquals(1, foundPizzas.size());
    }
}