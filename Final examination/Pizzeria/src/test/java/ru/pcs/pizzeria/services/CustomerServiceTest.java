package ru.pcs.pizzeria.services;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * pizzeria
 * 12.12.2021
 *
 * @author Anastasiya Zhalnina
 */
@DisplayName(value = "Customer Service is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@SpringBootTest
class CustomerServiceTest {
    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService = new CustomerServiceImpl(customerRepository);
    private final List<Customer> customers = new ArrayList<>();
    private Customer customerFirst;
    private Customer customerSecond;

    @BeforeEach
    public void init() {
        customerFirst = new Customer();
        customerFirst.setId(1);
        customerFirst.setFirstName("Иван");
        customerFirst.setLastName("Иванов");
        customerFirst.setTelephone("+70000000001");
        customerFirst.setEmail("1@mail.ru");
        customerFirst.setAddress("Москва");

        customerSecond = new Customer();
        customerSecond.setId(2);
        customerSecond.setFirstName("Ирина");
        customerSecond.setLastName("Сидорова");
        customerSecond.setTelephone("+70000000002");
        customerSecond.setEmail("2@mail.ru");
        customerSecond.setAddress("Москва");

        customers.add(customerFirst);
        customers.add(customerSecond);
    }

    @Test
    public void returning_customer_by_id() {
        Mockito.when(customerRepository.findById(2)).thenReturn(java.util.Optional.ofNullable(customerSecond));
        Customer found = customerService.getCustomerById(2);
        assertNotNull(found);
        assertEquals(customerSecond.getFirstName(), found.getFirstName());
        assertEquals(customerSecond.getId(), found.getId());
    }

    @Test
    public void delete_customer_by_id() {
        customerService.deleteCustomer(customerFirst.getId());
        Mockito.verify(customerRepository, Mockito.times(1))
                .deleteById(customerFirst.getId());
    }

    @Test
    public void returning_customer_by_email() {
        Mockito.when(customerRepository.findCustomersByEmail("2@mail.ru")).thenReturn(customerSecond);
        Customer found = customerService.getCustomerByEmail("2@mail.ru");
        assertNotNull(found);
        assertEquals(customerSecond.getFirstName(), found.getFirstName());
        assertEquals(customerSecond.getEmail(), found.getEmail());
    }
}