create table Pizza
(
    id          serial primary key,
    name        varchar(50),
    ingredients varchar(300),
    price       decimal,
    image       varchar(500)

);

insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/INDEIKA-V-MANDARINAK.png', 'Индейка в мандаринах',
        'Пастрами из индейки, соус альфредо, мандарины, цитрусовый соус, моцарелла, смесь сыров чеддер и пармезан',
        869);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/SYRNAY.png', 'Сырная', 'Моцарелла, сыры чеддер и пармезан, соус альфредо', 449);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/PEPPERONI-FRES.png', 'Пепперони фреш',
        'Пикантная пепперони, увеличенная порция моцареллы, томаты, томатный соус', 449);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/PESTO.png', 'Песто',
        'Цыпленок, соус песто, кубики брынзы, томаты, моцарелла, соус альфредо', 699);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/07/VETCINA-SYR.png', 'Ветчина и сыр', 'Ветчина, моцарелла, соус альфредо', 459);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/KARBONARA.png', 'Карбонара',
        'Бекон, сыры чеддер и пармезан, моцарелла, томаты, красный лук, чеснок, соус альфредо, итальянские травы',
        699);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/07/DVOINOI-TYPLENOK.png', 'Двойной цыпленок', 'Цыпленок, моцарелла, соус альфредо',
        459);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/MIKS.png', 'Микс',
        'Бекон, цыпленок, ветчина, сыр блю чиз, сыры чеддер и пармезан, соус песто, кубики брынзы, томаты, красный лук, моцарелла, соус альфредо, чеснок, итальянские травы',
        799);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/PEPPERONI.png', 'Пепперони',
        'Пикантная пепперони, увеличенная порция моцареллы, томатный соус', 649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/MARGARITA.png', 'Маргарита',
        'Увеличенная порция моцареллы, томаты, итальянские травы, томатный соус', 549);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/VETCINA-I-GRIBY.png', 'Ветчина и грибы',
        'Ветчина, шампиньоны, увеличенная порция моцареллы, томатный соус', 549);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/07/GAVAISKAY.png', 'Гавайская', 'Ветчина, ананасы, моцарелла, томатный соус', 649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/DIABLO.png', 'Диабло',
        'Острая чоризо, острый перец халапеньо, соус барбекю, митболы, томаты, сладкий перец, красный лук, моцарелла, томатный соус',
        699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/FIRMENNAY.png', 'Фирменная',
        'Бекон, митболы, пикантная пепперони, моцарелла, томаты, шампиньоны, сладкий перец, красный лук, чеснок, томатный соус',
        699);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/07/SYRNYI-TYPLENOK.png', 'Сырный цыпленок',
        'Цыпленок, моцарелла, сыры чеддер и пармезан, сырный соус, томаты, соус альфредо, чеснок',
        699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/TYPLENOK-RANC.png', 'Цыпленок ранч',
        'Цыпленок, ветчина, соус ранч, моцарелла, томаты, чеснок', 699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/MYSNAY.png', 'Мясная',
        'Цыпленок, ветчина, пикантная пепперони, острая чоризо, моцарелла, томатный соус', 699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/ARRIVA.png', 'Аррива!',
        'Цыпленок, острая чоризо, соус бургер, сладкий перец, красный лук, томаты, моцарелла, соус ранч, чеснок',
        649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/CETYRE-SEZONA.png', 'Четыре сезона',
        'Увеличенная порция моцареллы, ветчина, пикантная пепперони, кубики брынзы, томаты, шампиньоны, итальянские травы, томатный соус',
        649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/CIZBURGER-PITTA.png', 'Чизбургер-пицца',
        'Мясной соус болоньезе, соус бургер, соленые огурчики, томаты, красный лук, моцарелла',
        649);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/KOLBASKI-BARBEKY.png', 'Колбаски Барбекю',
        'Острая чоризо, соус барбекю, томаты, красный лук, моцарелла, томатный соус', 649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/FRISTAILO.png', 'Фристайло',
        'Томаты, сладкий перец, красный лук, соус песто, митболы, моцарелла, томатный соус', 649);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/OVOSI-I-GRIBY.png', 'Овощи и грибы',
        'Шампиньоны, томаты, сладкий перец, красный лук, маслины, кубики брынзы, моцарелла, томатный соус, итальянские травы',
        649);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/CETYRE-SYRA.png', 'Четыре сыра',
        'Сыр блю чиз, сыры чеддер и пармезан, моцарелла, соус альфредо', 699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/TYPLENOK-BARBEKY.png', 'Цыпленок барбекю',
        'Цыпленок, бекон, соус барбекю, красный лук, моцарелла, томатный соус', 699);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/04/DVOINAY-PEPPERONI.png', 'Двойная пепперони',
        'Двойная порция пикантной пепперони, увеличенная порция моцареллы, томатный соус',
        699);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/SUPERMYSNAY.png', 'Супермясная',
        'Пикантная пепперони, цыпленок, острая чоризо, бекон, митболы, моцарелла, томатный соус',
        799);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/04/PITTA-PIROG.png', 'Пицца-пирог', 'Ананасы, брусника, сгущенное молоко', 549);
insert into Pizza(image, name, ingredients, price)
values ('https://ic.wampi.ru/2021/12/07/NEZNYI-LOSOS.png', 'Нежный лосось',
        'Лосось, томаты, соус песто, моцарелла, соус альфредо', 849);
insert into Pizza(image, name, ingredients, price)
values ('https://ie.wampi.ru/2021/12/07/MEKSIKANSKAY.png', 'Мексиканская',
        'Цыпленок, острый перец халапеньо, соус сальса, томаты, сладкий перец, красный лук, моцарелла, томатный соус',
        699);

create table Customer
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    telephone  varchar(12),
    email      varchar(50) unique,
    address    varchar(100)
);

create table Basket
(
    id          serial primary key,
    customer_id integer,
    pizza_id    integer,
    foreign key (customer_id) references Customer (id),
    foreign key (pizza_id) references Pizza (id)
);



create table Orders
(
    id          serial primary key,
    date        timestamp,
    customer_id integer,
    amount      integer,
    foreign key (customer_id) references Customer (id)
);

create table list_order(
                           id serial primary key,
                           order_id integer,
                           pizza_id integer,
                           count integer,
                           amount integer,
                           foreign key (order_id) references Orders(id),
                           foreign key (pizza_id) references Pizza(id)
);

