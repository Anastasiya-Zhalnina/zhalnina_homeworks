package ru.pcs.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.pizzeria.model.Customer;

import java.util.Optional;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    /**
     * Поиск покупателя по e-mail
     *
     * @param email - e-mail покупателя
     * @return покупатель
     */
    Optional<Customer> findByEmail(String email);

    /**
     * Поиск покупателя по e-mail
     *
     * @param email - e-mail покупателя
     * @return покупатель
     */
    Customer findCustomersByEmail(String email);
}
