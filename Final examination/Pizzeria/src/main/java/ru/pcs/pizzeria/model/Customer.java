package ru.pcs.pizzeria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс покупателя
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Customer {

    public enum Role {
        ADMIN,
        CUSTOMER;
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;
    private String telephone;

    @Column(unique = true)
    private String email;
    private String address;
    private String hashPassword;

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.REMOVE})
    private List<Basket> basket;

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.REMOVE})
    private List<Orders> order;
}
