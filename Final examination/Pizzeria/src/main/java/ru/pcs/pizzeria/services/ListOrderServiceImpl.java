package ru.pcs.pizzeria.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.model.ListOrder;
import ru.pcs.pizzeria.repository.ListOrderRepository;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс отвечающий за формирование списка заказанных продуктов
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Component
@RequiredArgsConstructor
public class ListOrderServiceImpl implements ListOrderService {
    private final ListOrderRepository listOrderRepository;

    @Override
    public List<ListOrder> getListOrderByOrder_Id(Integer orderId) {
        return listOrderRepository.findListOrdersByOrder_Id(orderId);
    }
}
