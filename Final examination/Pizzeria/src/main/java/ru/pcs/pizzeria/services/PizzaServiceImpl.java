package ru.pcs.pizzeria.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.exseption.ObjectNotFoundException;
import ru.pcs.pizzeria.forms.PizzaForm;
import ru.pcs.pizzeria.model.Pizza;
import ru.pcs.pizzeria.repository.PizzaRepository;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, отвечающий за бизнес-логику добавления, обновления и удаления товара - пиццы.
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Component
public class PizzaServiceImpl implements PizzaService {
    private final PizzaRepository pizzaRepository;

    @Autowired
    public PizzaServiceImpl(PizzaRepository pizzaRepository) {
        this.pizzaRepository = pizzaRepository;
    }

    @Override
    public void addPizza(PizzaForm form) {
        Pizza pizza = Pizza.builder()
                .name(form.getName())
                .ingredients(form.getIngredients())
                .price(form.getPrice())
                .build();
        pizzaRepository.save(pizza);
    }

    @Override
    public List<Pizza> getAllPizzas() {
        return pizzaRepository.findAll();
    }

    @Override
    public void deletePizza(Integer pizzaId) {
        pizzaRepository.deleteById(pizzaId);
    }

    @Override
    public void updatePizza(Pizza pizza) {
        pizzaRepository.save(pizza);
    }

    @Override
    public Pizza getPizza(Integer id) {
        return pizzaRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
    }
}
