package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.pizzeria.forms.PizzaForm;
import ru.pcs.pizzeria.model.Pizza;

import ru.pcs.pizzeria.services.PizzaService;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей продукта
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/pizzas")
public class PizzaController {

    private final PizzaService pizzaService;

    @GetMapping
    public String getPizzasPage(Model model) {
        List<Pizza> pizzas = pizzaService.getAllPizzas();
        model.addAttribute("pizzas", pizzas);
        return "pizzas";
    }

    @GetMapping("/{pizza-id}")
    public String getPizzaPage(Model model, @PathVariable("pizza-id") Integer id) {
        Pizza pizza = pizzaService.getPizza(id);
        model.addAttribute("pizza", pizza);
        return "pizza";
    }

    @PostMapping
    public String addPizza(PizzaForm form) {
        pizzaService.addPizza(form);
        return "redirect:/pizzas";
    }

    @PostMapping("/{pizza-id}/delete")
    public String deletePizza(@PathVariable("pizza-id") Integer pizzaId) {
        pizzaService.deletePizza(pizzaId);
        return "redirect:/pizzas";
    }

    @PostMapping("/{pizza-id}/update")
    public String updatePizza(@PathVariable("pizza-id") Integer id, Pizza form) {
        Pizza pizza = Pizza.builder()
                .id(id)
                .name(form.getName())
                .ingredients(form.getIngredients())
                .price(form.getPrice())
                .image(form.getImage())
                .build();
        pizzaService.updatePizza(pizza);
        return "redirect:/pizzas";
    }
}
