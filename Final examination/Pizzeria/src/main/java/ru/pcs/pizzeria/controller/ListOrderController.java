package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.model.ListOrder;
import ru.pcs.pizzeria.services.CustomerService;
import ru.pcs.pizzeria.services.ListOrderService;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей заказанных продуктов
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/list_order")
public class ListOrderController {
    private final ListOrderService listOrderService;
    private final CustomerService customerService;

    @GetMapping
    public String getListOrdersPage() {
        return "list_order";
    }

    @GetMapping("/{order-id}")
    public String getListOrderPage(Model model, @PathVariable("order-id") Integer orderId) {
        List<ListOrder> listOrders = listOrderService.getListOrderByOrder_Id(orderId);
        model.addAttribute("listOrders", listOrders);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer customer = customerService.getCustomerByEmail(auth.getName());
        model.addAttribute("customer", customer);
        return "list_order";
    }
}
