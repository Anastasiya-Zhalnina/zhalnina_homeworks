package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.pizzeria.forms.CustomerForm;
import ru.pcs.pizzeria.model.Basket;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.model.Pizza;
import ru.pcs.pizzeria.services.BasketService;
import ru.pcs.pizzeria.services.CustomerService;
import ru.pcs.pizzeria.services.PizzaService;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей покупателя
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final PizzaService pizzaService;
    private final BasketService basketService;

    @GetMapping("/customer")
    public String getProfilePage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer customer = customerService.getCustomerByEmail(auth.getName());
        model.addAttribute("customer", customer);
        return "customer";
    }

    @GetMapping("/catalog")
    public String getCatalogPage() {
        return "catalog";
    }

    @GetMapping("/catalog/{customer-id}")
    public String getCatalogPageById(Model model, @PathVariable("customer-id") Integer customerId) {
        List<Pizza> pizzas = pizzaService.getAllPizzas();
        model.addAttribute("pizzas", pizzas);
        Customer customer = customerService.getCustomerById(customerId);
        model.addAttribute("customer", customer);
        model.addAttribute("customerId", customerId);
        List<Basket> baskets = basketService.getAllBaskets();
        model.addAttribute("baskets", baskets);
        return "catalog";
    }

    @GetMapping("/profile")
    public String getProfilePage() {
        return "profile";
    }

    @GetMapping("/profile/{customer-id}")
    public String getProfileCustomerPage(Model model, @PathVariable("customer-id") Integer customerId) {
        Customer customer = customerService.getCustomerById(customerId);
        List<Basket> baskets = basketService.getBasketByCustomer(customerId);
        model.addAttribute("customer", customer);
        model.addAttribute("baskets", baskets);
        return "profile";
    }

    @PostMapping("/profile/{customer-id}/update")
    public String updateCustomer(@PathVariable("customer-id") Integer id, CustomerForm form) {
        customerService.update(id, form);
        return "redirect:/profile/" + id;
    }

    @PostMapping("/profile/{customer-id}/delete")
    public String deleteCustomer(@PathVariable("customer-id") Integer customerId) {
        customerService.deleteCustomer(customerId);
        return "redirect:/signIn";
    }
}
