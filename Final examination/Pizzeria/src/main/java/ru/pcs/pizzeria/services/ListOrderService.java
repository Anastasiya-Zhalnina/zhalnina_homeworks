package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.model.ListOrder;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface ListOrderService {
    /**
     * Получить список заказанных продуктов
     *
     * @param orderId - идентификатор заказа
     * @return список заказов
     */
    List<ListOrder> getListOrderByOrder_Id(Integer orderId);
}
