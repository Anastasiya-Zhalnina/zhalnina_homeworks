package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.forms.PizzaForm;
import ru.pcs.pizzeria.model.Pizza;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface PizzaService {
    /**
     * Добавление товара - пиццы
     *
     * @param form - форма, с полями для добавления товара
     */
    void addPizza(PizzaForm form);

    /**
     * Получить список всех продуктов
     *
     * @return список продуктов
     */
    List<Pizza> getAllPizzas();

    /**
     * Удаление товара по идентификатору
     *
     * @param pizzaId - идентификатор товара
     */
    void deletePizza(Integer pizzaId);

    /**
     * Обновление информации по товару
     *
     * @param pizza - обновляемый товар
     */
    void updatePizza(Pizza pizza);

    /**
     * Получение товара по идентификатору
     *
     * @param id - идентификатор товара
     * @return найденный товар
     */
    Pizza getPizza(Integer id);
}
