package ru.pcs.pizzeria.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.forms.SignUpForm;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.repository.CustomerRepository;

/**
 * Pizzeria
 * 10.12.2021
 * Бизнес-логика реализации страницы регистрации пользователя
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final CustomerRepository customerRepository;

    @Override
    public void signUpCustomer(SignUpForm form) {
        Customer customer = Customer.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .telephone(form.getTelephone())
                .email(form.getEmail())
                .address(form.getAddress())
                .role(Customer.Role.CUSTOMER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();
        customerRepository.save(customer);
    }
}
