package ru.pcs.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.pizzeria.model.Pizza;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface PizzaRepository extends JpaRepository<Pizza, Integer> {
}
