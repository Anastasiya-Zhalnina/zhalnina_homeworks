package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.pizzeria.forms.SignUpForm;
import ru.pcs.pizzeria.services.SignUpService;

import javax.validation.Valid;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей регистрации
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/signUp")
public class SignUpController {
    private final SignUpService signUpService;

    @GetMapping
    public String getStringUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUpCustomer(@Valid SignUpForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Телефон вводится в формате +700000000000");
            return "redirect:/signUp";
        }
        signUpService.signUpCustomer(form);
        return "redirect:/signIn";
    }
}
