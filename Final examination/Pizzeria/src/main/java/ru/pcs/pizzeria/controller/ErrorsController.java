package ru.pcs.pizzeria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей ошибки
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
public class ErrorsController {
    @GetMapping("/error/404")
    public String get404Page() {
        return "errors/error_404";
    }
}
