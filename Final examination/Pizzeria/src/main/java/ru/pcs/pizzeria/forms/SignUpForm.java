package ru.pcs.pizzeria.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * Pizzeria
 * 10.12.2021
 * Форма для регистрации
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
public class SignUpForm {

    private String firstName;
    private String lastName;

    @Length(min = 12, max = 12)
    private String telephone;
    private String email;
    private String address;
    private String password;
}
