package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.model.Orders;

import java.util.List;

/**
 * pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface OrderService {
    /**
     * Добавление заказа покупателя
     * @param customerId - идентификатор покупателя
     */
    void addOrder(Integer customerId);

    /**
     * Получение списка заказов по идентификатору покупателя
     * @param customerId - идентификатор покупателя
     * @return список заказов покупателя
     */
    List<Orders> getOrderByCustomer(Integer customerId);
}
