package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.model.Basket;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface BasketService {
    /**
     * Добавление товара в корзину покупателя
     *
     * @param customerId - идентификатор покупателя
     * @param pizzaId    - идентификатор товара
     */
    void addPizzaToBasket(Integer customerId, Integer pizzaId);

    /**
     * Получение списка всех корзин
     *
     * @return список корзин
     */
    List<Basket> getAllBaskets();

    /**
     * Получение списка корзин по идентификатору покупателя
     *
     * @param customerId - идентификатор покупателя
     * @return - список корзин
     */
    List<Basket> getBasketByCustomer(Integer customerId);

    /**
     * Удаление корзины по идентификатору
     *
     * @param basketId - идентификатор корзины
     */
    void deleteBasket(Integer basketId);

    /**
     * Удаление корзин покупателя по идентификатору покупателя
     *
     * @param customerId - идентификатор покупателя
     */
    void deleteBasketsByCustomerId(Integer customerId);
}
