package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.forms.SignUpForm;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface SignUpService {
    /**
     * Добавление пользователя в систему.
     *
     * @param form - форма с полями для регистрации
     */
    void signUpCustomer(SignUpForm form);
}
