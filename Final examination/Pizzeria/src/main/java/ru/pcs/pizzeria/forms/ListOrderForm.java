package ru.pcs.pizzeria.forms;

import lombok.Data;
import ru.pcs.pizzeria.model.Orders;
import ru.pcs.pizzeria.model.Pizza;

/**
 * Pizzeria
 * 10.12.2021
 * Форма списка заказанных товаров
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
public class ListOrderForm {
    private Integer id;
    private Orders order;
    private Pizza pizza;
    private Integer count;
    private Integer amount;
}
