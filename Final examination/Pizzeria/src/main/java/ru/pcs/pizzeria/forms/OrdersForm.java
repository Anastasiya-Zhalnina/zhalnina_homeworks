package ru.pcs.pizzeria.forms;

import lombok.Data;
import ru.pcs.pizzeria.model.Customer;

import java.time.LocalDateTime;

/**
 * Pizzeria
 * 10.12.2021
 * Форма заказа
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
public class OrdersForm {
    private Integer id;
    private LocalDateTime date;
    private Customer customer;
    private Integer amount;
}
