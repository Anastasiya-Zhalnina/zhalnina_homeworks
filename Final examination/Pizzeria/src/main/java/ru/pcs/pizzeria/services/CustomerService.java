package ru.pcs.pizzeria.services;

import ru.pcs.pizzeria.forms.CustomerForm;
import ru.pcs.pizzeria.model.Customer;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface CustomerService {
    /**
     * Получить покупателя по идентификатору
     *
     * @param id - идентификатор покупателя
     * @return покупатель
     */
    Customer getCustomerById(Integer id);

    /**
     * Удаление покупателя по идентификатору покупателя
     *
     * @param customerId - идентификатор покупателя
     */
    void deleteCustomer(Integer customerId);

    /**
     * Обновление данных покупателя
     *
     * @param id   - идентификатор покупателя
     * @param form - форма с полями для обновления покупателя
     */
    void update(Integer id, CustomerForm form);

    /**
     * Получить покупателя по e-mail
     *
     * @param email - e-mail покупателя
     * @return покупателя
     */
    Customer getCustomerByEmail(String email);
}
