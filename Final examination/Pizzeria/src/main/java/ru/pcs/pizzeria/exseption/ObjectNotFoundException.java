package ru.pcs.pizzeria.exseption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Pizzeria
 * 10.12.2021
 * Класс исключения
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ObjectNotFoundException extends RuntimeException {
}