package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей входа
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/signIn")
public class SignInController {
    @GetMapping
    public String getSignInPage(Model model) {
        return "signIn";
    }
}
