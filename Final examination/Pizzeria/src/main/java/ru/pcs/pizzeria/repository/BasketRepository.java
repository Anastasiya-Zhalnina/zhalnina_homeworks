package ru.pcs.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.pizzeria.model.Basket;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface BasketRepository extends JpaRepository<Basket, Integer> {
    /**
     * Поиск списка корзин по идентификатору покупателя
     *
     * @param id - идентификатор покупателя
     * @return список корзин покупателя
     */
    List<Basket> findAllByCustomer_Id(Integer id);

    /**
     * Удаление корзин покупателя
     *
     * @param customerId - идентификатор покупателя
     */
    void deleteBasketsByCustomer_Id(Integer customerId);
}
