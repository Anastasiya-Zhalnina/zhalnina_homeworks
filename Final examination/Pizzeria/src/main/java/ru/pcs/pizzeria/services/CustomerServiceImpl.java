package ru.pcs.pizzeria.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.exseption.ObjectNotFoundException;
import ru.pcs.pizzeria.forms.CustomerForm;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.repository.CustomerRepository;

/**
 * Pizzeria
 * 10.12.2021
 * Класс отвечающий за бизнес-логику формирования покупателя
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Component
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
    }

    @Override
    public void deleteCustomer(Integer customerId) {
        customerRepository.deleteById(customerId);
    }

    @Override
    public void update(Integer id, CustomerForm form) {
        Customer customer = customerRepository.getById(id);
        customer.setFirstName(form.getFirstName());
        customer.setLastName(form.getLastName());
        customer.setAddress(form.getAddress());
        customer.setEmail(form.getEmail());
        customer.setTelephone(form.getTelephone());
        customerRepository.save(customer);
    }

    @Override
    public Customer getCustomerByEmail(String email) {
        return customerRepository.findCustomersByEmail(email);
    }
}
