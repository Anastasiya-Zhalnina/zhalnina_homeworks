package ru.pcs.pizzeria.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.model.*;
import ru.pcs.pizzeria.repository.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, отвечающий за бизнес-логику формирования заказа
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Component
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ListOrderRepository listOrderRepository;
    private final BasketRepository basketRepository;

    @Override
    public void addOrder(Integer customerId) {
        Customer customer = customerRepository.getById(customerId);
        List<Basket> baskets = basketRepository.findAllByCustomer_Id(customerId);
        Integer amount = 0;
        for (Basket basket : baskets) {
            Pizza pizza = basket.getPizza();
            amount += pizza.getPrice();
        }

        Orders order = Orders.builder()
                .date(LocalDateTime.now().withNano(0))
                .customer(customer)
                .amount(amount)
                .build();
        orderRepository.save(order);

        for (Basket basket : baskets) {
            ListOrder listOrder = ListOrder.builder()
                    .order(order)
                    .pizza(basket.getPizza())
                    .count(1)
                    .amount(basket.getPizza().getPrice())
                    .build();
            listOrderRepository.save(listOrder);
        }
    }

    @Override
    public List<Orders> getOrderByCustomer(Integer customerId) {
        return orderRepository.findAllByCustomer_Id(customerId);
    }
}
