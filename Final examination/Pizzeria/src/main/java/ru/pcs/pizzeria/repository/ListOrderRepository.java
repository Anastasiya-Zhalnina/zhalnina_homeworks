package ru.pcs.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.pizzeria.model.ListOrder;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface ListOrderRepository extends JpaRepository<ListOrder, Integer> {
    /**
     * Поиск списка заказанных продуктов
     *
     * @param orderId - идентификатор заказа
     * @return список заказанных продуктов
     */
    List<ListOrder> findListOrdersByOrder_Id(Integer orderId);
}
