package ru.pcs.pizzeria.forms;

import lombok.Data;

/**
 * Pizzeria
 * 10.12.2021
 * Форма покупателя
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
public class CustomerForm {
    private Integer id;
    private String firstName;
    private String lastName;
    private String telephone;
    private String email;
    private String address;
    private String password;
}
