package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.pizzeria.model.Basket;
import ru.pcs.pizzeria.services.BasketService;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей корзины
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
public class BasketController {
    private final BasketService basketService;

    @GetMapping("/basket")
    public String getBasketsPage(Model model) {
        List<Basket> baskets = basketService.getAllBaskets();
        model.addAttribute("baskets", baskets);
        return "basket";
    }

    @GetMapping("/catalog/{customer-id}/basket")
    public String getBasketPage(Model model, @PathVariable("customer-id") Integer customerId) {
        List<Basket> baskets = basketService.getBasketByCustomer(customerId);
        model.addAttribute("baskets", baskets);
        model.addAttribute("customerId", customerId);
        return "basket";
    }

    @PostMapping("/catalog/{customer-id}/{pizza-id}")
    public String addPizzaToBasket(@PathVariable("customer-id") Integer customerId,
                                   @PathVariable("pizza-id") Integer pizzaId) {
        basketService.addPizzaToBasket(customerId, pizzaId);
        return "redirect:/catalog/" + customerId;
    }

    @PostMapping("/basket/{basket-id}/delete")
    public String deleteBasket(@PathVariable("basket-id") Integer basketId, @RequestParam("customerId") Integer customerId) {
        basketService.deleteBasket(basketId);
        return "redirect:/catalog/" + customerId + "/basket";
    }
}
