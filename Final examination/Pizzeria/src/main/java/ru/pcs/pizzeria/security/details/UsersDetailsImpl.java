package ru.pcs.pizzeria.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pcs.pizzeria.model.Customer;

import java.util.Collection;
import java.util.Collections;

/**
 * Pizzeria
 * 10.12.2021
 * Предоставление необходимой информации для построения объекта аутентификации
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public class UsersDetailsImpl implements UserDetails {

    private final Customer customer;

    public UsersDetailsImpl(Customer customer) {
        this.customer = customer;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = customer.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return customer.getHashPassword();
    }

    @Override
    public String getUsername() {
        return customer.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
