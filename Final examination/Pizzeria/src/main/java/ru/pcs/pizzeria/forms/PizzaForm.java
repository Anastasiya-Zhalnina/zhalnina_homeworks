package ru.pcs.pizzeria.forms;

import lombok.Data;

/**
 * Pizzeria
 * 10.12.2021
 * Форма продукта
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
public class PizzaForm {
    private String name;
    private String ingredients;
    private Integer price;
    private String image;
}
