package ru.pcs.pizzeria.forms;

import lombok.Data;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.model.Pizza;

/**
 * Pizzeria
 * 10.12.2021
 * Форма корзины
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */

@Data
public class BasketForm {
    private Integer id;
    private Customer customer;
    private Pizza pizza;
}
