package ru.pcs.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.pizzeria.model.Orders;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
public interface OrderRepository extends JpaRepository<Orders, Integer> {
    /**
     * Поиск всех заказов по идентификатору покупателя
     *
     * @param customerId - идентификатор покупателя
     * @return список заказов покупателя
     */
    List<Orders> findAllByCustomer_Id(Integer customerId);
}
