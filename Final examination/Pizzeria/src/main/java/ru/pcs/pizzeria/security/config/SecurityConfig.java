package ru.pcs.pizzeria.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Pizzeria
 * 10.12.2021
 * Реализация безопасности в конфигурации
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsServiceImpl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/pizzas").hasAuthority("ADMIN")
                .antMatchers("/pizzas/**").hasAuthority("ADMIN")
                .antMatchers("/customer").authenticated()
                .antMatchers("/catalog").authenticated()
                .antMatchers("/catalog/**").authenticated()
                .antMatchers("/profile").authenticated()
                .antMatchers("/profile/**").authenticated()
                .antMatchers("/basket").authenticated()
                .antMatchers("/basket/**").authenticated()
                .antMatchers("/list_order").authenticated()
                .antMatchers("/list_order/**").authenticated()
                .antMatchers("/order").authenticated()
                .antMatchers("/order/**").authenticated()
                .antMatchers("/signUp").permitAll()
                .and()
                .formLogin()
                .loginPage("/signIn")
                .usernameParameter("email")
                .passwordParameter("password")
                .defaultSuccessUrl("/customer")
                .permitAll();
    }
}

