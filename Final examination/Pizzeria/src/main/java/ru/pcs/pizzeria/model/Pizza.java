package ru.pcs.pizzeria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс продукта
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Pizza {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String ingredients;
    private Integer price;
    private String image;

    @OneToMany(mappedBy = "pizza", cascade = {CascadeType.REMOVE})
    private List<Basket> basket;

    @OneToMany(mappedBy = "pizza", cascade = {CascadeType.REMOVE})
    private List<ListOrder> listOrder;
}
