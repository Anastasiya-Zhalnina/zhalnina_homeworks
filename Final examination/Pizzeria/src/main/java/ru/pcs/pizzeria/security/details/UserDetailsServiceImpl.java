package ru.pcs.pizzeria.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.repository.CustomerRepository;

/**
 * Pizzeria
 * 10.12.2021
 * Извлечение пользователя из базы
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@RequiredArgsConstructor
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private final CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Пользователь не найден"));
        return new UsersDetailsImpl(customer);
    }
}
