package ru.pcs.pizzeria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс заказов
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    private Integer amount;

    @OneToMany(mappedBy = "order", cascade = {CascadeType.REMOVE})
    private List<ListOrder> listOrder;
}
