package ru.pcs.pizzeria.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.pizzeria.model.Basket;
import ru.pcs.pizzeria.model.Customer;
import ru.pcs.pizzeria.model.Pizza;
import ru.pcs.pizzeria.repository.BasketRepository;
import ru.pcs.pizzeria.repository.CustomerRepository;
import ru.pcs.pizzeria.repository.PizzaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс отвечающий за формирование корзины
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Component
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {
    private final BasketRepository basketRepository;
    private final CustomerRepository customerRepository;
    private final PizzaRepository pizzaRepository;

    @Override
    public void addPizzaToBasket(Integer customerId, Integer pizzaId) {
        Customer customer = customerRepository.getById(customerId);
        Pizza pizza = pizzaRepository.getById(pizzaId);
        Basket basket = Basket.builder()
                .customer(customer)
                .pizza(pizza)
                .build();
        basketRepository.save(basket);
    }

    @Override
    public List<Basket> getAllBaskets() {
        return basketRepository.findAll();
    }

    @Override
    public List<Basket> getBasketByCustomer(Integer customerId) {
        return basketRepository.findAllByCustomer_Id(customerId);
    }

    @Override
    public void deleteBasket(Integer basketId) {
        basketRepository.deleteById(basketId);
    }

    @Override
    @Transactional
    public void deleteBasketsByCustomerId(Integer customerId) {
        basketRepository.deleteBasketsByCustomer_Id(customerId);
    }
}
