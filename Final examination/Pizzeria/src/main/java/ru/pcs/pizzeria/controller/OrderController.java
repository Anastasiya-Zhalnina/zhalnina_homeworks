package ru.pcs.pizzeria.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.pizzeria.model.Orders;
import ru.pcs.pizzeria.services.BasketService;
import ru.pcs.pizzeria.services.OrderService;

import java.util.List;

/**
 * Pizzeria
 * 10.12.2021
 * Класс, обрабатывающий HTTP-запросы со страницей заказа
 *
 * @author Anastasiya Zhalnina
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;
    private final BasketService basketService;

    @GetMapping
    public String getOrderPage() {
        return "order";
    }

    @GetMapping("/{customer-id}")
    public String getOrderPageByCustomer(Model model, @PathVariable("customer-id") Integer customerId) {
        List<Orders> orders = orderService.getOrderByCustomer(customerId);
        model.addAttribute("orders", orders);
        model.addAttribute("customerId", customerId);
        return "order";
    }

    @PostMapping("/{customer-id}")
    public String addOrder(@PathVariable("customer-id") Integer customerId) {
        orderService.addOrder(customerId);
        return "redirect:/order/" + customerId;
    }

    @PostMapping("/{customer-id}/delete")
    public String deleteBasket(@PathVariable("customer-id") Integer customerId) {
        basketService.deleteBasketsByCustomerId(customerId);
        return "redirect:/catalog/" + customerId;
    }
}
