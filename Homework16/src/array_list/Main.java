package array_list;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> line = new ArrayList<>();
        line.add("Hello");
        line.add("Java");
        line.add("Homework");
        line.add("List");
        line.add("Array");
        line.add("Learning");
        line.add("University");
        line.add("Cool");

        line.removeAt(5);

        for (int i = 0; i < line.size(); i++) {
            System.out.print(line.get(i) + " ");
        }
        System.out.println();

        ArrayList<Integer> number = new ArrayList<>();
        number.add(45);
        number.add(78);
        number.add(10);
        number.add(17);
        number.add(89);
        number.add(16);

        number.removeAt(3);
        for (int i = 0; i < number.size(); i++) {
            System.out.print(number.get(i) + " ");
        }
    }
}
