package array_list;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add(T element) {
        if (isFullArray()) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {
        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void clear() {
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public void removeAt(int index) {
        int newSize = 0;
        T[] newElements = (T[]) new Object[size];
        int i = 0;
        for (int j = 0; j < size; j++) {
            if (j != index) {
                newElements[i] = elements[j];
                i++;
                newSize++;
            }
        }
        this.size = newSize;
        this.elements = newElements;
    }
}
