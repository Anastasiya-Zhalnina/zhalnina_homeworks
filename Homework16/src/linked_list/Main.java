package linked_list;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> number = new LinkedList<>();
        number.add(34);
        number.add(120);
        number.add(-10);
        number.add(11);
        number.add(50);
        number.add(100);
        number.add(99);

        for (int i = 0; i < number.size(); i++) {
            System.out.print(number.get(i) + " ");
        }
        System.out.println();


        LinkedList<String> line = new LinkedList<>();
        line.add("Hello");
        line.add("Java");
        line.add("Homework");
        line.add("List");
        line.add("Array");
        line.add("Learning");
        line.add("University");
        line.add("Cool");

        for (int i = 0; i < line.size(); i++) {
            System.out.print(line.get(i) + " ");
        }

    }
}
