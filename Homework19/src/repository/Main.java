package repository;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        List<User> usersFoundByAge = usersRepository.findByAge(17);
        System.out.println("Пользователи, соответствующие заданному возрасту:");
        System.out.printf("%10s| %7s|%17s\n", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersFoundByAge) {
            System.out.printf("%10s| %7d|%17s\n", user.getName(), user.getAge(), user.isWorker());
        }

        System.out.println("**************************************************");
        List<User> usersIsWorking = usersRepository.findByIsWorkerIsTrue();
        System.out.println("          Работающие пользователи:");
        System.out.printf("%10s| %7s|%17s\n", "Имя", "Возраст", "Работающий да/нет");
        for (User user : usersIsWorking) {
            System.out.printf("%10s| %7d|%17s\n", user.getName(), user.getAge(), user.isWorker());
        }
    }
}
