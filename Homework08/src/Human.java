public class Human {
    private String name;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (this.weight < 0) {
            this.weight = 0;
        }
        this.weight = weight;
    }
}
