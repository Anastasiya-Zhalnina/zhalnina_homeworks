public class Main {

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setName("Human_" + (i + 1));
            humans[i].setWeight((Math.random() * 96) + 4);
        }
        sortByWeight(humans);
        System.out.printf("%8s| %6s|\n", "Human", "Weight");
        for (int i = 0; i < humans.length; i++) {
            System.out.printf("%8s| %6.2f|\n", humans[i].getName(), humans[i].getWeight());
        }
    }

    public static void sortByWeight(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            double minWeight = humans[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < minWeight) {
                    minWeight = humans[j].getWeight();
                    minIndex = j;
                }
            }
            double tempWight = humans[i].getWeight();
            String tempName = humans[i].getName();
            double minimumWight = humans[minIndex].getWeight();
            String minimumName = humans[minIndex].getName();
            humans[i].setWeight(minimumWight);
            humans[i].setName(minimumName);
            humans[minIndex].setWeight(tempWight);
            humans[minIndex].setName(tempName);
        }
    }
}
