package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.model.Product;
import ru.pcs.web.repository.ProductsRepository;

@Controller
public class ProductController {
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") Double price,
                             @RequestParam("count") Integer count) {
        Product product = Product.builder()
                .description(description)
                .price(price)
                .count(count)
                .build();

        productsRepository.save(product);

        return "redirect:/products_add.html";
    }
}
