package ru.pcs.web.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.model.Product;


import javax.sql.DataSource;

import java.util.List;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product(description, price, count) values(?, ?, ?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from product order by id";
    //language=SQL
    private static final String SQL_FIND_ALL_BY_PRICE = "select * from product where price = ?";
    //language=SQL
    private static final String SQL_FIND_ALL_BY_ORDERS_COUNT = "select * from product as p left join (select product_id, sum(count) as count from Orders group by product_id) as o on p.id = o.product_id where o.count = ?";

    private JdbcTemplate jdbcTemplate;

    private static final RowMapper<Product> productRowMapper = ((row, rowNum) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int count = row.getInt("count");
        return new Product(id, description, price, count);
    });

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getCount());
    }
}
