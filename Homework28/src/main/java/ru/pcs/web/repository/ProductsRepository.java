package ru.pcs.web.repository;

import ru.pcs.web.model.Product;

import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    List<Product> findAllByOrdersCount(int ordersCount);

    void save(Product product);
}
