import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ведите последовательность чисел (все числа в диапазоне от -100 до 100):");
        byte number = scanner.nextByte();
        int[] count = new int[201];
        while (number != -1) {
            count[number + 100]++;
            number = scanner.nextByte();
        }
        int minNumber = getMinNumber(count);
        System.out.println("Число, которое присутствует в последовательности минимальное количество раз равно: " + minNumber);
    }

    private static int getMinNumber(int[] count) {
        int min = 2_147_483_647;
        int minNumber = 0;
        for (int i = 0; i < count.length; i++) {
            if (count[i] != 0 && count[i] < min) {
                min = count[i];
                minNumber = i - 100;
            }
        }
        return minNumber;
    }
}
