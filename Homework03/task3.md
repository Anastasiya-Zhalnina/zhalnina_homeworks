### Задача

```
Пусть есть последовательность чисел 
a0, a1, a2, ..., aN
N -> infinity, aN -> -1
Описать решение следующей задачи (аналогично описанию на уроке):

Вывести количество локальных минимумов:
ai - локальный минимум, если ai-1 > ai < ai+1

46
23 -> локальный минимум
55
120
33 -> локальный минимум
400
21 -> локальный минимум
500

Ответ: 3
```

### Решение

```
CЧИТАТЬ -> a
count  = 0
previous = a
next
ПОКА a != -1
    СЧИТАТЬ -> next
    ЕСЛИ a < previous И а < next:
        count++
        
    previous = a
    a = next
    
ВЫВЕСТИ -> count    
```

### Решение на языке Java

```
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        int a = scanner.nextInt();
        int previous = a;
        int next;
        while (a != -1) {
            next = scanner.nextInt();
            if (a < previous && a < next) {
                count++;
            }
            previous = a;
            a = next;
        }
        System.out.println(count);
    }
}
```